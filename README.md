# Instalation   
You'll need:    
- NodeJS/npm   
- Ionic  
- Cordova  

#Running   
To run the web version (It's possible to use the mobile version on browser):   
`$ ionic serve`  
  
To create the APK:  
`$ ionic cordova build android`   
