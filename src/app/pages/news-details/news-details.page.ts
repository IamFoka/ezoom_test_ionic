import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.page.html',
  styleUrls: ['./news-details.page.scss'],
})
export class NewsDetailsPage implements OnInit {

  information = null;
  result = null;

  constructor(private activatedRoute: ActivatedRoute, private newsService: NewsService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id')

    this.result = this.newsService.getDetails(id);
    this.result.subscribe((res) => {
      this.result = res.news;
      this.information = res.news[0]
    })
  }

}
