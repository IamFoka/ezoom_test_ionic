import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  results: Observable<any>;

  constructor(private newsService: NewsService) { }

  ngOnInit() {
  }

  loadData(){
    this.results = this.newsService.searchData();
    this.results.subscribe((res) => {
      this.results = res.news
    })
  }

  ionViewWillEnter() {
    this.loadData()
  }

  doRefresh(event) {
    this.loadData()

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
