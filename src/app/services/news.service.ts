import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  url = 'http://localhost:8080/index.php/rest/news'

  constructor(private http: HttpClient) { }

  searchData(): Observable<any> {
    let data = this.http.get(`${this.url}`).pipe();

    return data
  }

  getDetails(id) {
    let data = this.http.get(`${this.url}?id=${id}`).pipe()

    return data
  }
}
